var createError = require('http-errors');
/* middleware main object */
var express = require('express');
/* path util */
var path = require('path');
var favicon = require('serve-favicon');
/* parser */
var cookieParser = require('cookie-parser');
/* noBots */
var noBots = require('express-nobots');
/* session */
var session = require('express-session');
/* secure */
var helmet = require('helmet');
/* locale */
var locale = require('locale');

// var indexRouter = require('./routes/index');

/* route */
var comsRoutes = require('./routes/coms') || {};
var adminRoutes = require('./routes/admin') || {};
var systemRoutes = require('./routes/system') || {};

var app = express();

app.use(session({
  secret: 'sdjkfejDOFJ3#%$^',  //keyboard cat
  resave: false,
  saveUninitialized: true
}))

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

/* uncomm ent after placing your favicon in /public */
app.use(favicon(path.join(__dirname,'public/images','favicon.ico')));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

/* secure */
app.use(noBots({
	block: true
}));
app.use(express.static(path.join(__dirname, 'public')));

// app.use('/', indexRouter);

comsRoutes.setup({
  'comsControllers': {
    'index': require('./controllers/coms/index') || {},
    'sign': require('./controllers/coms/sign') || {},
    'schedule': require('./controllers/coms/schedule') || {},
    'monitoring': require('./controllers/coms/monitoring') || {},
    'attendance': require('./controllers/coms/attendance') || {},
    'ranking': require('./controllers/coms/ranking') || {},
    'fee': require('./controllers/coms/fee') || {},
    'mypage': require('./controllers/coms/mypage') || {},
    'address': require('./controllers/coms/address') || {},
  },
  'app': app
});
adminRoutes.setup({
  'adminControllers': {
    'budget': require('./controllers/admin/budget') || {},
    'member': require('./controllers/admin/member') || {},
  },
  'app': app
});
systemRoutes.setup({
  'systemControllers': {
    'system': require('./controllers/system/system') || {},
  },
  'app': app
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


module.exports = app;
