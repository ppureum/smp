/**
 * Schedule Mapper
 */
module.exports = {

  get: {
    getScheduleAll: function() {
      return "  SELECT s.seq AS schedule_seq  " + 
      "  	, `date`  " + 
      "  	, `time`  " + 
      "  	, p.seq AS place_seq  " + 
      "  	, p.place " + 
      "  	, (SELECT group_concat(m.name SEPARATOR ',')  " + 
      " 		FROM schedule_members sm " + 
      " 		INNER JOIN members m " + 
      " 		ON sm.member_seq = m.seq " + 
      " 		WHERE schedule_seq = s.seq " +
      "     AND sm.attend_yn = 'Y') AS members " + 
      "  FROM schedules s  " + 
      "  INNER JOIN places p  " + 
      "  ON s.place_seq = p.seq  " + 
      "  ORDER BY s.seq DESC  " + 
      "  LIMIT :start, :end ;  "; 
    },
    getPlaceAll: function() {
      return " SELECT seq AS place_seq " + 
      " 	, place " + 
      " FROM places; "; 
    },
    getMemberAll: function() {
      return " SELECT group_concat(m.name SEPARATOR ',') members " + 
      " FROM schedule_members sm " + 
      " INNER JOIN members m " + 
      " ON sm.member_seq = m.seq " + 
      " WHERE schedule_seq = :schedule_seq " + 
      " AND sm.attend_yn = 'Y'; "; 
    }

  },
  post: {
    addSchedule: function() {
      return " INSERT INTO schedules ( " + 
      " 	`date`, " + 
      " 	`time`, " + 
      " 	place_seq, " + 
      " 	created_member_seq, " + 
      " 	updated_member_seq, " + 
      " 	created_time, " + 
      " 	updated_time " + 
      " ) VALUES ( " + 
      " 	:date, " + 
      " 	:time, " + 
      " 	:place_seq, " + 
      " 	:member_seq, " + 
      " 	:member_seq, " + 
      " 	now(), " + 
      " 	now() " + 
      " ) "; 
    },
    modifySchedule: function() {
      return " UPDATE schedules " + 
      " SET date = :date " + 
      " 	, time = :time " + 
      " 	, place_seq = :place_seq " + 
      " 	, updated_member_seq = :member_seq " + 
      " 	, updated_time = now() " + 
      " WHERE seq = :schedule_seq ";
    },
    removeSchedule: function() {
      return " DELETE FROM schedules WHERE seq = :seq; ";
    },
    removeScheduleMember: function() {
      return " DELETE FROM schedule_members WHERE schedule_seq = :seq; ";
    },
    addMember: function() {
      return " INSERT INTO schedule_members ( " + 
      " 	member_seq " + 
      " 	, schedule_seq " + 
      " 	, attend_yn " + 
      " 	, created_time " + 
      " 	, updated_time " + 
      " ) VALUES ( " + 
      " 	:member_seq " + 
      " 	, :schedule_seq " + 
      " 	, :attend_yn " + 
      " 	, now() " + 
      " 	, now() " + 
      " ) "; 
    },
    modifyMember: function() {
      return " UPDATE schedule_members " + 
      " SET attend_yn = :attend_yn " + 
      " WHERE schedule_seq = :schedule_seq " + 
      " AND member_seq = :member_seq; ";
    }
  }


};
