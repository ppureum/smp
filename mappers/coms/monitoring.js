/**
 * Monitoring Mapper
 */
module.exports = {

  get: {
    getMonitoringAll: function() {
      return " SELECT seq " + 
      " 	, title " + 
      " 	, youtube_link " + 
      " 	, created_member_seq " + 
      " 	, created_time " + 
      " FROM monitorings " + 
      " ORDER BY seq DESC  " + 
      " LIMIT :start, :end; "; 
    }
  },
  post: {
    addMonitoring: function() {
      return " INSERT INTO monitorings ( " + 
      " 	title " + 
      " 	, youtube_link " + 
      " 	, created_member_seq " + 
      " 	, updated_member_seq " + 
      " 	, created_time " + 
      " 	, updated_time " + 
      " ) VALUES ( " + 
      " 	:title " + 
      " 	, :link " + 
      " 	, :member_seq " + 
      " 	, :member_seq " + 
      " 	, now() " + 
      " 	, now() " + 
      " ) "; 
    },
    removeMonitoring: function() {
      return "DELETE FROM monitorings WHERE seq = :seq";
    }
  }


};
