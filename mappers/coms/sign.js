/**
 * Sign Mapper
 */
module.exports = {

  get: {
    getMemberById: function() {
      _sql = "  SELECT m.seq  " + 
      "  	, id  " + 
      "   , password  " + 
      "  	, name  " + 
      "  	, phone  " + 
      "   , authenticate_yn  " + 
      "   , mt.type AS member_type  " + 
      "  FROM members m " + 
      "  LEFT JOIN member_types mt " + 
      "  ON m.member_type_seq = mt.seq " + 
      "  WHERE id = :id  "; 
      return _sql;
    },

  },
  post: {
    join: function() {
      _sql = " INSERT INTO members ( " + 
      " 	id " + 
      " 	, password " + 
      " 	, name " + 
      " 	, phone " + 
      " 	, created_time " + 
      " 	, updated_time " + 
      " ) " + 
      " VALUES ( " + 
      " 	:id " + 
      " 	, :password " + 
      "   , :name " + 
      " 	, :phone " + 
      " 	, now() " + 
      " 	, now() " + 
      " ) "; 

      return _sql;
    }
  }


};
