/**
 * COMS Monitoring Controller
 */
var sqlFactory = require('../../lib/db/sqlFactory');
var monitoringMapper = require('../../mappers/coms/monitoring');

module.exports = {

  // GET /monitoring
  getMonitoring: function(req, res, next) {		
    console.log(" ===== monitoring page ===== ");

    return new Promise(
      function(resolve, reject) {
        sqlFactory.select({
          sql: monitoringMapper.get.getMonitoringAll(),
          replacements: {
            start: 0,
            end: 3
          },
          callback: function(result) {
            resolve(result);
          }
        });
      }
    ).then(function(result) {
      res.render('coms' + '/monitoring', {
        monitoring_list: result || []
      });
    });
  },
  // GET /monitoring/more/:start
  getMonitoringMore: function(req, res, next) {
    var start = Number(req.params.start);
    var end = start + 3;
    
    new Promise(
      function(resolve, reject) {
        sqlFactory.select({
          sql: monitoringMapper.get.getMonitoringAll({}),
          replacements: {
            start: start,
            end: end
          },
          callback: function(result) {
            resolve(result);
          }
        });
      }
    ).then(function(list) {
      res.send(list);
    });
  },
  // POST /monitoring/:crud
  postMonitoring: function(req, res, next) {		
    var crud = req.params.crud;
    var data = req.body;
    var user = req.session.user;

    switch (crud) {
      case 'create':
        new Promise((resolve, reject) => {
          sqlFactory.insert({
            sql: monitoringMapper.post.addMonitoring(),
              replacements: {
                title: data.title,
                link: data.link,
                member_seq: user.seq
              },
              callback: function(result) {
                res.send('done');
              },
              error: function(e) {
                console.log("error ::::" + e);
              }
          });
        });
        break;
      case 'delete': 
        new Promise((resolve, reject) => {
          sqlFactory.delete({
            sql: monitoringMapper.post.removeMonitoring(),
              replacements: {
                seq: data.seq
              },
              callback: function(result) {
                res.send('done');
              },
              error: function(e) {
                console.log("error ::::" + e);
              }
          });
        });
        break;
    }
  }
};