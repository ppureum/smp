/**
 * COMS Schedule Controller
 */
var sqlFactory = require('../../lib/db/sqlFactory');
var scheduleMapper = require('../../mappers/coms/schedule');
 
module.exports = {

  // GET /schedule
  getSchedule: function(req, res, next) {		
    console.log(" ===== schedule page ===== ");
    var user = req.session.user;

    //일정 조회 
    new Promise(
      function(resolve, reject) {
        sqlFactory.select({
          sql: scheduleMapper.get.getScheduleAll({}),
          replacements: {
            start: 0,
            end: 10
          },
          callback: function(result) {
            resolve(result);
          }
        });
      }
    ).then(function(schedules) {
      //장소 조회 
      return new Promise(
        function(resolve, reject) {
          sqlFactory.select({
            sql: scheduleMapper.get.getPlaceAll(),
            replacements: {},
            callback: function(result) {
              resolve(result);
            }
          });
        }
      ).then(function(places) {
        res.render('coms' + '/schedule', {
          schedules: schedules || [],
          places: places || [],
          user: user || {}
        });
      });
    });
  },
  // GET /schedule/more/:start
  getScheduleMore: function(req, res, next) {
    var start = Number(req.params.start);
    var end = start + 10;
    
    //일정 조회 
    new Promise(
      function(resolve, reject) {
        sqlFactory.select({
          sql: scheduleMapper.get.getScheduleAll({}),
          replacements: {
            start: start,
            end: end
          },
          callback: function(result) {
            resolve(result);
          }
        });
      }
    ).then(function(schedules) {
      res.send(schedules);
    });
  },
  // POST /schedule/:crud
  postSchedule: function(req, res, next) {
    var crud = req.params.crud;
    var data = req.body;
    var user = req.session.user;

    switch (crud) {
      case 'create':
        console.log("==== create new schedule ====");
        new Promise(
          function(resolve, reject) {
            sqlFactory.insert({
              sql: scheduleMapper.post.addSchedule(),
              replacements: {
                date: data.date,
                time: data.time,
                place_seq: data.place_seq,
                member_seq: user.seq
              },
              callback: function(result) {
                resolve();
              },
              error: function(e) {
                reject(e);
              }
            });
        }).then(function() {
          res.send('done');
        }).catch(function(e) {
          console.log("error ::::" + e);
        });
        break;
      case 'update': 
        console.log("==== update schedule ====");
        new Promise(
          function(resolve, reject) {
            sqlFactory.update({
              sql: scheduleMapper.post.modifySchedule(),
              replacements: {
                date: data.date,
                time: data.time,
                place_seq: data.place_seq,
                member_seq: user.seq,
                schedule_seq: data.schedule_seq
              },
              callback: function(result) {
                resolve();
              },
              error: function(e) {
                reject(e);
              }
            });
        }).then(function() {
          res.send('done');
        }).catch(function(e) {
          console.log("error ::::" + e);
        });
        break;
      case 'delete': 
        console.log("==== delete schedule ====");
        new Promise(
          function(resolve, reject) {
            sqlFactory.delete({
              sql: scheduleMapper.post.removeScheduleMember(), //멤버삭제
              replacements: {
               seq: data.schedule_seq
              },
              callback: function(result) {
                resolve();
              },
              error: function(e) {
                reject(e);
              }
            });
        }).then(function() {
          return new Promise((resolve, reject) => {
            sqlFactory.delete({
              sql: scheduleMapper.post.removeSchedule(), //일정삭제
              replacements: {
               seq: data.schedule_seq
              },
              callback: function(result) {
                res.send('done');
              },
              error: function(e) {
                console.log("error ::::" + e);
              }
            });
          });
        }).catch(function(e) {
          console.log("error ::::" + e);
        });
        break;
    }
  },

  // POST /schedule/member/:crud
  postScheduleMember: function(req, res, next) {
    var crud = req.params.crud;
    var data = req.body;
    var user = req.session.user;

    switch (crud) {
      case 'create':
        new Promise((resolve, reject) => {
          sqlFactory.insert({
            sql: scheduleMapper.post.addMember(), 
            replacements: {
              schedule_seq: data.schedule_seq,
              member_seq: user.seq,
              attend_yn: 'Y'
            },
            callback: function(result) {
              resolve();
            },
            error: function(e) {
              reject();
            }
          });
        }).then(function() {
          return new Promise((resolve, reject) => {
            //참석 멤버 조회 
            sqlFactory.select({
              sql: scheduleMapper.get.getMemberAll(), 
              replacements: {
                schedule_seq: data.schedule_seq,
              },
              callback: function(result) {
                var _members = result[0].members;
                res.send(_members);
              },
              error: function(e) {
                console.log("error ::::" + e);
              }
            });
          });
        }).catch(function(e) {
          console.log("error ::::" + e);
        });
        break;
      case 'update': 
        new Promise((resolve, reject) => {
          sqlFactory.update({
            sql: scheduleMapper.post.modifyMember(), 
            replacements: {
              schedule_seq: data.schedule_seq,
              member_seq: user.seq,
              attend_yn: 'N'
            },
            callback: function(result) {
              resolve();
            },
            error: function(e) {
              reject();
            }
          });
        }).then(function() {
          return new Promise((resolve, reject) => {
            //참석 멤버 조회 
            sqlFactory.select({
              sql: scheduleMapper.get.getMemberAll(), 
              replacements: {
                schedule_seq: data.schedule_seq,
              },
              callback: function(result) {
                var _members = result[0].members;
                res.send(_members);
              },
              error: function(e) {
                console.log("error ::::" + e);
              }
            });
          });
        }).catch(function(e) {
          console.log("error ::::" + e);
        });
        break;
    }
  }
};