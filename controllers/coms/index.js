/**
 * COMS Index Controller
 */
module.exports = {

  // GET /
  getRoot: function(req, res, next) {		
    res.redirect('/login');
    // res.render('coms' + '/index', {
    // });
  },

  // GET /report-error
  getReportError: function(req, res, next) {		
    res.render('coms' + '/report-error', {
    });
  }

};