/**
 * COMS Sign Controller
 */
var sqlFactory = require('../../lib/db/sqlFactory');
var signMapper = require('../../mappers/coms/sign');
 
module.exports = {

  // GET /login
  getLogin: function(req, res, next) {		
    console.log(" ===== login page ===== ");
    res.render('coms' + '/login', {
    });
  },

  // POST /login
  postLogin: function(req, res, next) {		
    var _id = req.body.id;
    var _password = req.body.password;

    new Promise(
      function(resolve, reject) {
        sqlFactory.select({
          sql: signMapper.get.getMemberById(),
          replacements: {
            id: _id
          },
          callback: function(result) {
            var _user = result[0] || null;
           
            resolve(_user);
          }
        });
    }).then(function(user) {
      if(!user) { // 없는 계정 
       res.send({success: false, reason: 'not-found'});
      } else if(user.authenticate_yn === 'N') { // 권한 없음 
        res.send({success: false, reason: 'auth-error'});
      } else if(user.password !== _password) { //패스워드 확인  TODO 암,복호화 
        res.send({success: false, reason: 'password-error'});
      } else {
        user.password = undefined;
        
        //세션 저장 
        req.session.user = user;
        req.session.save(() => {
          res.send({success: true});
        })
      }
    });
  },

  // GET /join
  getJoin: function(req, res, next) {		
    console.log(" ===== join page ===== ");
    res.render('coms' + '/join', {
    });
  },

  // POST /join
  postJoin: function(req, res, next) {
    var _info = req.body;

    new Promise(
      function(resolve, reject) {
      //아이디 중복 확인 
      sqlFactory.select({
        sql: signMapper.get.getMemberById(),
        replacements: {
          id: _info.id
        },
        callback: function(result) {
          if(result.length === 0) resolve(_info);
          else reject('duplication-error');
        }
      });
    }).then(function(info){
      //회원가입 처리 
      sqlFactory.insert({
        sql: signMapper.post.join(),
        replacements: info,
        callback: function(result) {
          res.send({
            success: true
          });
        }
      });
    }).catch(function(reason) {
      res.send({
        success: false,
        reason: reason
      });
    }); 
  },

  // GET /change-password
  getChangePW: function(req, res, next) {		
    console.log(" ===== change password page ===== ");
    res.render('coms' + '/change-password', {
    });
  },

  // GET /withdrawal
  getWithdrawal: function(req, res, next) {		
    console.log(" ===== withdrawal page ===== ");
    res.render('coms' + '/withdrawal', {
    });
  },

  // GET /complete-withdrawal
  getCompleteWithdrawal: function(req, res, next) {		
    console.log(" ===== complete withdrawal page ===== ");
    res.render('coms' + '/complete-withdrawal', {
    });
  }
};