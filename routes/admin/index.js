/**
 * ADMIN routes
 */
exports.setup = function (params) {

  var _app = params.app;
  var _controllers = params['admin' + 'Controllers'];
   
  //--------------- member management page ----------------//
  _app.get('/admin/member-management',  _controllers.member.getMemberManagement);

  //--------------- membership fee management page ----------------//
  _app.get('/admin/membership-fee-management',  _controllers.budget.getMembershipFeeManagement);


};