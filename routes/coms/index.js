var auth = require('../../lib/auth');

/**
 * COMS routes
 */
exports.setup = function (params) {

  var _app = params.app;
  var _controllers = params['coms' + 'Controllers'];
   
  //--------------- index page ----------------//
  _app.get('/',  _controllers.index.getRoot);

  //--------------- login page ----------------//
  _app.get('/login',  _controllers.sign.getLogin);
  _app.post('/login',  _controllers.sign.postLogin);
  _app.get('/join',  _controllers.sign.getJoin);
  _app.post('/join',  _controllers.sign.postJoin);

  //--------------- schedule page ----------------//
  _app.get('/schedule', _controllers.schedule.getSchedule);
  _app.get('/schedule/more/:start', _controllers.schedule.getScheduleMore);
  _app.post('/schedule/:crud', _controllers.schedule.postSchedule);
  _app.post('/schedule/member/:crud', _controllers.schedule.postScheduleMember);

  //--------------- monitoring page ----------------//
  _app.get('/monitoring',  _controllers.monitoring.getMonitoring);
  _app.get('/monitoring/more/:start', _controllers.monitoring.getMonitoringMore);
  _app.post('/monitoring/:crud',  _controllers.monitoring.postMonitoring);

  //--------------- membership fee page ----------------//
  _app.get('/membership-fee',  _controllers.fee.getMembershipFee);

  //--------------- contact number page ----------------//
  _app.get('/contact-number',  _controllers.address.getContactNumber);

  //--------------- mypage page ----------------//
  _app.get('/mypage',  _controllers.mypage.getMypage);

  //--------------- change password page ----------------//
  _app.get('/change-password',  _controllers.sign.getChangePW);

  //--------------- withdrawal page ----------------//
  _app.get('/withdrawal',  _controllers.sign.getWithdrawal);
  _app.get('/complete-withdrawal',  _controllers.sign.getCompleteWithdrawal);

  //--------------- report error page ----------------//
  _app.get('/report-error',  _controllers.index.getReportError);

  //--------------- attendance page ----------------//
  _app.get('/attendance',  _controllers.attendance.getAttendance);

};