var Sequelize = require("sequelize");
var env = require('../../env/mysql_db');
var sequelize = new Sequelize(env.database, env.username, env.password, env.conn_obj);
var db = {};

db.sequelize = sequelize;

module.exports = db;
