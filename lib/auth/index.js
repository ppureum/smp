module.exports = {

  /**
   *  sign-in middleware
   *  passport 로그인 체크 / 세션X => 로그인 페이지로 redirect
   */
  checkAuthenticated:  function(req, res, next) {

    new Promise(
      function(resolve, reject) {
        console.log('===== _checkAuthenticated =====');
        console.log(req.user);
        
        if(req.user) resolve();
        else reject();
    })
    .then(function () {
      console.log("here !!!");
      return true;
    })
    .catch(function() {
      res.redirect('/login');
    });
  }

};