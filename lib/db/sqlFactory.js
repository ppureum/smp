var comsM = require('../../models/coms');

/**
 * SQL Factory
 */
module.exports = {

    // select
    select: function(params) {

        var _sql = params.sql || '';
        var _replacements = params.replacements || {};
        var _type = comsM.sequelize.QueryTypes.SELECT || {};
        var _callback = params.callback || this._callback;
        var _error = params.error || this._error;

        if (typeof _callback === 'function') {
            comsM.sequelize.query(_sql, {
                replacements: _replacements,
                type: _type
            }).then(_callback).catch(_error);
        } else {
            comsM.sequelize.query(_sql, {
                replacements: _replacements,
                type: _type
            }).catch(_error);
        }

    },

    // insert
    insert: function(params) {

        var _sql = params.sql || '';
        var _replacements = params.replacements || {};
        var _type = comsM.sequelize.QueryTypes.INSERT || {};
        var _callback = params.callback || this._callback;
        var _error = params.error || this._error;

        if (typeof _callback === 'function') {
            comsM.sequelize.query(_sql, {
                replacements: _replacements,
                type: _type
            }).then(_callback).catch(_error);
        } else {
            comsM.sequelize.query(_sql, {
                replacements: _replacements,
                type: _type
            }).catch(_error);
        }

    },

    // update
    update: function(params) {

        var _sql = params.sql || '';
        var _replacements = params.replacements || {};
        var _type = comsM.sequelize.QueryTypes.UPDATE || {};
        var _callback = params.callback || this._callbacks;
        var _error = params.error || this._error;

        if (typeof _callback === 'function') {
            comsM.sequelize.query(_sql, {
                replacements: _replacements,
                type: _type
            }).then(_callback).catch(_error);
        } else {
            comsM.sequelize.query(_sql, {
                replacements: _replacements,
                type: _type
            }).catch(_error);
        }
    },

    // delete
    delete: function(params) {

        var _sql = params.sql || '';
        var _replacements = params.replacements || {};
        var _type = comsM.sequelize.QueryTypes.DELETE || {};
        var _callback = params.callback || this._callbacks;
        var _error = params.error || this._error;

        if (typeof _callback === 'function') {
            comsM.sequelize.query(_sql, {
                replacements: _replacements,
                type: _type
            }).then(_callback).catch(_error);
        } else {
            comsM.sequelize.query(_sql, {
                replacements: _replacements,
                type: _type
            }).catch(_error);
        }

    },

    // procedure
    // sql	= "CALL sp_test( '"+currentDate+"', '"+currentDate+"', @o_err_yn, @o_err_msg );";
    procedure: function(params) {
        var _sql = params.sql || '';
        var _callback = params.callback || this._callbacks;
        var _error = params.error || this._error;

        if (typeof _callback === 'function') {
            comsM.sequelize.query(_sql).then(_callback).catch(_error);
        } else {
            comsM.sequelize.query(_sql).catch(_error);
        }

    },

    // view
    view: function(params) {

    },

    // trigger
    trigger: function(params) {

    },

    // default cabllback
    _callback: function(result) {
        console.log('===== Not defined callback function !!! =========================');
        throw '===== Not defined callback function !!! =========================';
    },

    // default cabllback
    _callbacks: function(result, flag) {
        console.log('===== Not defined callback function !!! =========================');
        throw '===== Not defined callback function !!! =========================';
    },

    // error
    _error: function(error) {
        console.log('===== Not defined error function !!! =========================');
        console.log(error);
        throw error;
    },

    _close: function(result) {
      comsM.sequelize.close();
    }
};
